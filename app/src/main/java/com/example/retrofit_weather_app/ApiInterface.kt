package com.example.retrofit_weather_app

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {
    //url:https://api.openweathermap.org/data/2.5/weather?q=Bolu&appid=0c204b4e35bac43071aa0e36c40b9547&lang=tr&units=metric
    //onemli

    @GET("data/2.5/weather")
    fun allList(@Query("q") input:String, @Query("appid") appid:String, @Query("lang") lang:String, @Query("units") units:String):Call<WeatherList>

}