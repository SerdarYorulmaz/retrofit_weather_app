package com.example.retrofit_weather_app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    val API_KEY="0c204b4e35bac43071aa0e36c40b9547"
    var cityName="Antalya"
    var comingData:WeatherList?=null
    var comingList: List<WeatherList.Weather>?=null
    var myAdapter:WeatherListAdapter?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var myLayoutManager=LinearLayoutManager(this@MainActivity,LinearLayoutManager.VERTICAL,false)
        recyclerWeaterList.layoutManager=myLayoutManager

        var apiInterface=ApiClient.client?.create(ApiInterface::class.java) //java tabanli oldugu icin
        var temp=apiInterface?.allList("${cityName}",API_KEY,"tr","metric")

        //kuyruga aliyoruz
        temp?.enqueue(object : Callback<WeatherList>{
            override fun onFailure(call: Call<WeatherList>, t: Throwable) {
                Log.e("HATA","${t.printStackTrace()}")
            }

            override fun onResponse(call: Call<WeatherList>, response: Response<WeatherList>) {
                Log.e("BAŞARILI MI: ","${call?.request()?.url()?.toString()}")
                comingData=response?.body()
                comingList=comingData?.weather

                myAdapter= WeatherListAdapter(comingList)
                recyclerWeaterList.adapter=myAdapter


                supportActionBar?.setSubtitle("ToplamListe: "+comingList?.size)

                Log.e("SONUC: ","Toplam Liste Sayısı: ${comingList?.size}")

//                for (i in 0..response?.body()?.weather?.size!!-1)
//                Toast.makeText(this@MainActivity, "Gelen Data: " + data?.weather?.get(i)?.id?.toString(), Toast.LENGTH_LONG)

              // Log.e("SONUC: ","${response?.body()?.weather?.get(0)?.description.toString()}")
            }

        })
    }
}
