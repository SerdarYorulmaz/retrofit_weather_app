package com.example.retrofit_weather_app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.only_line_list.view.*

class WeatherListAdapter(allList: List<WeatherList.Weather>?) :
    RecyclerView.Adapter<WeatherListAdapter.ViewHolder>() {
    var allListData = allList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherListAdapter.ViewHolder {

        var inflater=LayoutInflater.from(parent.context)
        var onlyList=inflater.inflate(R.layout.only_line_list,parent,false)

        return ViewHolder(onlyList) //burdaki return asagida onBindViewHolder a holder olarak geliyor
    }

    override fun getItemCount(): Int {
     return   allListData!!.size //null deger almiyor
    }

    override fun onBindViewHolder(holder: WeatherListAdapter.ViewHolder, position: Int) {

        var currentLineSatir=allListData?.get(position)
        holder.setData(currentLineSatir,position)
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            var onlyLine= itemView as CardView
            var tempTitle= onlyLine.txtListTitle


        fun setData(temp: WeatherList.Weather?, posion:Int){
            var tempImg=temp?.icon
            tempTitle.text=temp?.description


            Picasso.with(onlyLine.context).load("http://openweathermap.org/img/wn/${tempImg.toString()}@2x.png").into(onlyLine.imgStar)

        }
    }

}