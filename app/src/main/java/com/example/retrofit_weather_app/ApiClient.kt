package com.example.retrofit_weather_app

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

//url:https://api.openweathermap.org/data/2.5/weather?q=Bolu&appid=0c204b4e35bac43071aa0e36c40b9547&lang=tr&units=metric

//singleton
object ApiClient {

    var Base_Url="https://api.openweathermap.org/"
    private var retrofit:Retrofit?=null

    val client:Retrofit?
    get(){
        if(retrofit==null){
            retrofit=Retrofit.Builder()
                .baseUrl(Base_Url)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        }
        return retrofit
    }

}